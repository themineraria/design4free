"use strict";

const http = require("http");
const fs = require("fs");
const path = require("path");
const zlib = require('zlib');
const { MongoClient } = require("mongodb");
const uri ="mongodb://localhost:27017";
const client = new MongoClient(uri);

const port = 5000;
const root = "./src/";
const mime_types = getMimeTypes();

async function main() {
    try {
        const requestListener = function (req, res) {

            var method = req.method;
            var url = req.url;
            var request_path = decodeURI(url.replace(/^\/+/, "").replace(/\?.*$/, ""));
            if (request_path === "")
                request_path = "index.html";

            var request_args = {};
            decodeURI(url.replace(/^.*\?/, "")).split('&').forEach(elem => request_args[elem.split("=")[0]] = elem.split("=")[1]);

            var file_path = path.resolve(root, request_path);
            var mime_type = mime_types[path.extname(file_path)] || "application/octet-stream";

            if(request_path === "search") {
                // Parse var to int
                var start = parseInt(request_args["start"]);
                var length = parseInt(request_args["length"]);
                var code_postal = parseInt(request_args["code_postal"]);
                var nom_commune = request_args["nom_commune"];

                var cursor = null;

                if (!isNaN(code_postal)) {
                    cursor = collection.find( { COM : code_postal } ).skip( start ).limit( length );
                } else if (nom_commune != "") {
                    cursor = collection.find( { "\ufeffLibcom" : nom_commune } ).skip( start ).limit( length );
                } else {
                    cursor = collection.find().skip( start ).limit( length );
                }

                cursor.count().then(function (length_cursor) {

					cursor.toArray().then(function(result){
						var data_to_send = {
							"recordsTotal": length_cursor,
							"recordsFiltered": length_cursor,
							"data": result
						};
						//console.log(data_to_send);
						sendData(res, JSON.stringify(data_to_send), mime_type, method, url, request_path);
						// sendData(res, JSON.stringify(database["data"][4]), method, url, request_path);
					});
                });
            } else {
                fs.readFile(file_path, function(err, content) {
                    if (err) sendNotFound(res, method, url, request_path);
                    else sendData(res, content, mime_type, method, url, request_path);
                });
            }
        }

        http.createServer(requestListener).listen(port, function() {
            console.log("server running in '" + root + "' started on port " + port  +".\n");
        });

        await client.connect();
        const db = await client.db('d4g');
        const collection = await db.collection('data');
        console.log("Connected successfully to server");

    } finally {}
}

main().catch(console.dir);

function sendData(res, content, mime_type, method, url, request_path) {
    logResponse(method, url, 200, "Listing directory " + (request_path === "/" ? "." : request_path), "text/html");
    res.setHeader("Content-Encoding", "gzip");
    res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    res.setHeader("Pragma", "no-cache");
    res.setHeader("Expires", "0");
    res.setHeader("Content-Type", mime_type + "; charset=utf-8");
    zlib.gzip(content, function (_, result) {
        res.end(result);
    });
}

function sendNotFound(res, method, url, request_path) {
    var content = "<!DOCTYPE html><html lang=\"fr\"><head><meta charset=\"utf-8\"><title>404 Not Found.</title></head><body><h1>404 Not Found.</h1></body></html>";

    logResponse(method, url, 404, "File " + request_path + " not found");

    res.setHeader("Content-Encoding", "gzip");
    res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    res.setHeader("Pragma", "no-cache");
    res.setHeader("Expires", "0");
    res.setHeader("Content-Type", "text/html; charset=utf-8");
    res.writeHead(404);
    zlib.gzip(content, function (_, result) {
        res.end(result);
    });
}

function logResponse(method, url, code, message, mime_type) {
    if (mime_type) {
        message += " (" + mime_type + ")";
    }
	fs.appendFileSync('log.txt',(method + " " + url + " : " + code + ":", message + "\n"), 'utf8'); 
}

function getMimeTypes() {
    return {
        ".avi":  "video/avi",
        ".bmp":  "image/bmp",
        ".css":  "text/css",
        ".gif":  "image/gif",
        ".svg": "image/svg+xml",
        ".htm": "text/html",
        ".html": "text/html",
        ".ico":  "image/x-icon",
        ".jpeg": "image/jpeg",
        ".jpg":  "image/jpeg",
        ".js": "text/javascript",
        ".json": "application/json",
        ".mov":  "video/quicktime",
        ".mp3":  "audio/mpeg3",
        ".mpa":  "audio/mpeg",
        ".mpeg": "video/mpeg",
        ".mpg":  "video/mpeg",
        ".oga": "audio/ogg",
        ".ogg": "application/ogg",
        ".ogv": "video/ogg",
        ".pdf":  "application/pdf",
        ".png":  "image/png",
        ".tif":  "image/tiff",
        ".tiff": "image/tiff",
        ".txt":  "text/plain",
        ".wav":  "audio/wav",
        ".xml":  "text/xml",
    };
}



