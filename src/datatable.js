function clearTable() { 
	datatable.clear(); codePostal = null; nomCommune= null;
}
var codePostal = null; 
var nomCommune= null; 
var datatable = null; 

$("#submit").click(function() {
  nomCommune = $( "#nomCommune" ).val();
  codePostal = $( "#codePostal" ).val();
  $('#datatable').DataTable().ajax.reload();
});

$(document).ready(

function () { 
datatable = $('#datatable').DataTable( { 
buttons: ['pdf' ,'csv'],
dom: 'Bfrtip',
serverSide: true, 
ordering: false, 
searching: false, 
ajax: { "url" : '/search', 
	"contentType": "application/json", "type": "GET", "data": function(d){
		return { "start": d.start, "length": d.length, "code_postal": codePostal, "nom_commune": nomCommune
		}
	}
},
scrollY: 200,
scroller: { loadingIndicator: true },
         /* columnDefs: [ { width: "100%", targets: 0 } ], */ "columns": [ {title: "Nom Commune", data: "\ufeffLibcom", "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) { $(nTd).html(oData.COM + " - " + sData);
                }, orderable: true}, {title: "Score AIN", data: "ACCÈS AUX INTERFACES NUMERIQUES region *  ", 
            orderable: true}, {title: "Score AI", data: "ACCES A L'INFORMATION region * ", orderable: true}, {title: "Score CA", data: "COMPETENCES ADMINISTATIVES region * ", orderable: true}, {title: "Score CNS", data: 
            "COMPÉTENCES NUMÉRIQUES / SCOLAIRES region * ", orderable: true}, {title: "Score Global", data: "SCORE GLOBAL region * ", orderable: true},
         ], /* "columns": [ { title: "User", "data": "EMAIL", "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) { $(nTd).html("<a href='#' data-target='#adminChat' data-correspondent='"+sData+"' 
                    data-order='"+(oData.ORDER_STATUS!==null ? oData.ORDER_STATUS:'none')+"' data-toggle='modal'>"+sData+"</a>");
                }
            },
            { title: "Number&nbsp;of&nbsp;messages", "data": "NB_MESSAGES" }, { title: "Order", "data": "ORDER_STATUS", "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) { if (sData !== null) $(nTd).html(sData); else 
                        $(nTd).html("none");
                }
            },
        ], */ "order": [[ 0, "asc" ], [ 1, "desc" ]]
    });
});
